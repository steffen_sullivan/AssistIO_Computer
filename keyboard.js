/**
@author Steffen Sullivan
*/

const robot = require('robotjs');

module.exports.typeString = function(stringToType) {
  robot.typeString(stringToType);
}
