/**

@author Steffen Sullivan
@version 11-23-2017

*/

const keyboard = require('./keyboard.js');

module.exports = computer;

function computer(query) {
  query = query.split(" ");
  if (query[0] == 'type') {
    query.shift();
    query = query.join(" ");
    keyboard.typeString(query);
  }
  else {
    return {
      'ttsText': 'Uknown command'
    }
  }
}
